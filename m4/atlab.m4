## Copyright (C) 2020 M. Oliveira, 2023 K. Ashwin Kumar
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
## 02110-1301, USA.
##
##

AC_DEFUN([ACX_ATLAB], [
AC_REQUIRE([ACX_YAML])
AC_REQUIRE([ACX_BLAS])
acx_atlab_ok=no

dnl Check if the library was given in the command line
dnl if not, use environment variables or defaults
AC_ARG_WITH(atlab-prefix, [AS_HELP_STRING([--with-atlab-prefix=DIR], [Directory where bigdft-atlab was installed.])])

# Set FCFLAGS_ATLAB only if not set from environment
if test x"$FCFLAGS_ATLAB" = x; then
  case $with_atlab_prefix in
    "") FCFLAGS_ATLAB="$ax_cv_f90_modflag/usr/include" ;;
    *)  FCFLAGS_ATLAB="$ax_cv_f90_modflag$with_atlab_prefix/include" ;;
  esac
fi

AC_ARG_WITH(atlab-include, [AS_HELP_STRING([--with-atlab-include=DIR], [Directory where bigdft-atlab Fortran headers were installed.])])
case $with_atlab_include in
  "") ;;
  *)  FCFLAGS_ATLAB="$ax_cv_f90_modflag$with_atlab_include" ;;
esac

dnl Backup LIBS and FCFLAGS
acx_atlab_save_LIBS="$LIBS"
acx_atlab_save_FCFLAGS="$FCFLAGS"


dnl The test
AC_MSG_CHECKING([for bigdft-atlab])

testprogram="AC_LANG_PROGRAM([],[[
  use f_harmonics
  type(f_multipoles) :: mp
  call f_multipoles_create(mp,2)


]])"


FCFLAGS="$FCFLAGS_ATLAB $acx_atlab_save_FCFLAGS"

if test -z "$LIBS_ATLAB"; then
  if test ! -z "$with_atlab_prefix"; then
    LIBS_ATLAB="-L$with_atlab_prefix/lib"
  else
    LIBS_ATLAB=""
  fi
  LIBS_ATLAB="$LIBS_ATLAB -latlab-1"
fi

# Use LIBS_FUTILE with LIBS_ATLAB as the test program needs both
LIBS="$LIBS_ATLAB $LIBS_FUTILE $LIBS_LIBYAML $LIBS_BLAS $acx_atlab_save_LIBS"

AC_LINK_IFELSE($testprogram, [acx_atlab_ok=yes], [])

AC_MSG_RESULT([$acx_atlab_ok ($FCFLAGS_ATLAB $LIBS_ATLAB)])


dnl Finally, execute ACTION-IF-FOUND/ACTION-IF-NOT-FOUND:
if test x"$acx_atlab_ok" = xyes; then
  AC_DEFINE(HAVE_ATLAB, 1, [Defined if you have the bigdft-atlab library.])
else
  AC_MSG_WARN([Could not find bigdft-atlab library.])
  LIBS_ATLAB=""
  FCFLAGS_ATLAB=""
fi

AC_SUBST(LIBS_ATLAB)
AC_SUBST(FCFLAGS_ATLAB)
LIBS="$acx_atlab_save_LIBS"
FCFLAGS="$acx_atlab_save_FCFLAGS"

])dnl ACX_ATLAB
