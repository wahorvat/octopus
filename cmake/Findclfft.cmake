#[==============================================================================================[
#                                  clfft compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# Findclfft

clfft compatibility module for Octopus

This file is specifically tuned for Octopus usage. See `Octopus_FindPackage` for a more general
interface.

]===]

# Early exit if module already found and defined
# These files do not contain components, so early-exiting is acceptable
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND AND TARGET clfft::clfft)
    return()
endif ()

list(APPEND CMAKE_MESSAGE_CONTEXT Findclfft)
include(Octopus)
Octopus_FindPackage(${CMAKE_FIND_PACKAGE_NAME}
        NAMES clfft
        PKG_MODULE_NAMES clfft)

# Create appropriate aliases
if (${CMAKE_FIND_PACKAGE_NAME}_PKGCONFIG)
    add_library(clfft::clfft ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
endif ()
if(${CMAKE_FIND_PACKAGE_NAME}_FOUND)
    set(HAVE_CLFFT 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
        URL https://github.com/clMathLibraries/clFFT
        DESCRIPTION "clFFT is a software library containing FFT functions written in OpenCL"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
