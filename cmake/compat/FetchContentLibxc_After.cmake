if (NOT TARGET Libxc::xcf03)
	add_library(Libxc::xcf03 ALIAS xcf03)
endif ()
set_package_properties(Libxc PROPERTIES
		URL https://gitlab.com/libxc/libxc
		DESCRIPTION "Library of exchange-correlation functionals for density-functional theory."
)
set_property(GLOBAL APPEND PROPERTY PACKAGES_FOUND Libxc)
get_property(_packages_not_found GLOBAL PROPERTY PACKAGES_NOT_FOUND)
list(REMOVE_ITEM _packages_not_found Libxc)
set_property(GLOBAL PROPERTY PACKAGES_NOT_FOUND ${_packages_not_found})
