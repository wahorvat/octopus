foreach (test IN ITEMS
		01-hydrogen
		02-neon_mpi
		03-He-Hartree-Fock
		04-lithium
		09-morse
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
