# -*- coding: utf-8 mode: shell-script -*-

Test       : Magnons with generalized Bloch theorem - 1D H chain
Program    : octopus
TestGroups : short-run, periodic_systems
Enabled    : Yes

Processors : 4

Input      : 31-magnon_1d.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

Precision: 6.20e-08
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -5.26258284
Precision: 3.92e-07
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; 0.00000000
Precision: 8.72e-08
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -5.36593948
Precision: 1.43e-07
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 0.00111243
Precision: 7.82e-08
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -0.36082426
Precision: 6.71e-08
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ;  -0.00624233
Precision: 1.65e-08
match ;    Kinetic energy      ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 0.19038095
Precision: 5.43e-07
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -5.08700961

Precision: 1.00e-04
match ;    k-point 2 (x)     ; GREPFIELD(static/info, '#k =       2', 7) ; 0.01
Precision: 1.63e-05
match ;    Eigenvalue  1     ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -5.542966

Input      : 31-magnon_1d.02-td.inp
Precision: 5e-07
match ;     Total magnet. [step  99]     ; LINEFIELD(td.general/total_magnetization, -2, 3) ; 1.5074366260284910e-02
Precision: 5e-07
match ;     Total magnet. [step  99]     ; LINEFIELD(td.general/total_magnetization, -2, 4) ; -4.0032474648971084e-03
Precision: 5e-07
match ;     Total magnet. [step 100]     ; LINEFIELD(td.general/total_magnetization, -1, 3) ; 1.4284097761605427e-02
Precision: 5e-07
match ;     Total magnet. [step 100]     ; LINEFIELD(td.general/total_magnetization, -1, 4) ; -3.8804944186251428e-03
Precision: 6.59e-09
match ;      Energy       [step  50]      ; LINEFIELD(td.general/energy, -51, 3) ; -5.2625828160430954e+00
Precision: 6.47e-09
match ;      Energy       [step 100]      ; LINEFIELD(td.general/energy, -1, 3) ; -5.2612115826921970e+00

Util : oct-spin_susceptibility
Input : 31-magnon_1d.02-td.inp

Precision: 4.37e-04
match ;    Spin susceptibilty Re [omega=2.7eV]    ; LINEFIELD(td.general/spin_susceptibility_q001, 30, 2) ; -0.120286E+01
Precision: 8.71e-05
match ;    Spin susceptibilty Im [omega=2.7eV]    ; LINEFIELD(td.general/spin_susceptibility_q001, 30, 3) ; -0.277159E+01
