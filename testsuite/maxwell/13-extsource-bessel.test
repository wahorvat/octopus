# -*- coding: utf-8 mode: shell-script -*-

Test       : External Source-TDDFT simulation with Bessel Beams
Program    : octopus
TestGroups : long-run, maxwell
Enabled    : Yes

#This test checks the dynamics of C atom under the influence of external bessel beam

# ground state
Input      : 13-extsource-bessel.01-carbon-gs.inp
Precision  : 4e-08
match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1
Precision: 1e-04
match ;     Initial energy     ; GREPFIELD(static/info, 'Total       =', 3) ; -5.39272780

# external bessel source governed dynamics, C placed at origin of beam
Input      : 13-extsource-bessel.02-carbon-td.inp
Precision  : 1e-4

match ;  Carbon Energy [step 20]  ; LINEFIELD(C/td.general/energy, -1, 3) ; -5.3905278025694043e+00 

Precision  : 1e-10
match ; Carbon Multipoles [step  15] ; LINEFIELD(C/td.general/multipoles, -6, 4) ; -4.7723595433505639e-01 
Precision: 3e-07
match ; Carbon Multipoles [step 20]  ; LINEFIELD(C/td.general/multipoles, -1, 4) ; -4.6960412510114247e-01 

Precision: 1e-8
match ;  Carbon Dipole Ex Field from External Source [step 15]   ; LINEFIELD(C/td.general/maxwell_dipole_field, -6, 3) ;  1.9421217562104071e-01 
match ;  Carbon Dipole Ey Field from External Source [step 20]   ; LINEFIELD(C/td.general/maxwell_dipole_field, -1, 4) ;  3.6848062695197131e-01 

Precision: 1e-8
match ;  External Source x component [step 15]   ; LINEFIELD(ExternalSource/bessel_source_at_point, -6, 2) ; 0.19468351 
# external bessel source governed dynamics, beam is shifted and enveloped
Input      : 13-extsource-bessel.03-enveloped-shifted-bessel.inp
Precision  : 1e-4

match ;  Carbon Energy [step 20]  ; LINEFIELD(C/td.general/energy, -1, 3) ; -5.3204584019281356e+00

Precision: 1e-8
match ;  Carbon Dipole Ex Field from External Source [step 15]   ; LINEFIELD(C/td.general/maxwell_dipole_field, -6, 3) ; -5.2792808615880882e-01
match ;  Carbon Dipole Ex Field from External Source [step 20]   ; LINEFIELD(C/td.general/maxwell_dipole_field, -1, 3) ; 0.0

Precision: 1e-8
match ;  External Source x component [step 10]   ; LINEFIELD(ExternalSource/bessel_source_at_point, -11, 2) ; -2.87544316 
match ;  External Source x component [step 15]   ; LINEFIELD(ExternalSource/bessel_source_at_point, -6, 2) ; 0.0
match ;  External Source y component [step 15]   ; LINEFIELD(ExternalSource/bessel_source_at_point, -6, 3) ; 0.0