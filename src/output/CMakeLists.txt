target_sources(Octopus_lib PRIVATE
		output.F90
		output_linear_medium.F90
		output_low.F90
		output_me.F90
		output_modelmb.F90
		output_mxll.F90
		output_berkeleygw.F90
		)
if (TARGET BerkleyGW::berkleygw)
	target_link_libraries(Octopus_lib PRIVATE BerkleyGW::berkleygw)
endif ()
if (TARGET etsf_io::etsf_io)
	target_link_libraries(Octopus_lib PRIVATE etsf_io::etsf_io)
endif ()
