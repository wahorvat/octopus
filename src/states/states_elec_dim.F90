!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module handles spin dimensions of the states and the k-point distribution.
!!
!! Currently, for SPIN_POLARIZED calculations, the spin channels are combined with the
!! k-point index. This module provides functions to extract the separate spin and k-point
!! indices.
!
module states_elec_dim_oct_m
  use debug_oct_m
  use distributed_oct_m
  use electron_space_oct_m
  use global_oct_m
  use kpoints_oct_m
  use math_oct_m
  use messages_oct_m
  use multicomm_oct_m
  use namespace_oct_m
  use profiling_oct_m

  implicit none

  private

  public ::                           &
    states_elec_dim_t,                &
    states_elec_dim_copy,             &
    states_elec_dim_end,              &
    is_spin_down,                     &
    is_spin_up

  !> Spin-polarized k-indices for non-periodic systems.
  integer, public, parameter :: &
    SPIN_DOWN = 1,              &
    SPIN_UP   = 2

  ! TODO(Alex) Issue #672. Remove kpts indices and weights from states_elec_dim_t
  !> @brief class for organizing spins and k-points
  type states_elec_dim_t
    ! Components are public by default
    integer :: dim                  !< Dimension of the state
    !!                                  (1 for UNPOLARIZED or SPIN_POLARIZED,
    !!                                   2 for SPINORS)
    integer :: ispin                !< @brief Spin mode (UNPOLARIZED, SPIN_POLARIZED, SPINORS)
    !!
    !!                                 set in states_elec_oct_m::states_elec_init from input variable SpinComponents
    integer :: nspin                !< Dimension of rho: 1 (UNPOLARIZED, 2 (SPIN_POLARIZED) or 4(SPINOR))
    !!
    !!                                 set in states_elec_oct_m::states_elec_init
    integer :: spin_channels        !< 2 if spin is considered, 1 if not.
    type(distributed_t) :: kpt           !< k-points including their parallelization information

  contains
    procedure :: get_spin_index => states_elec_dim_get_spin_index
    procedure :: get_kpoint_index => states_elec_dim_get_kpoint_index
  end type states_elec_dim_t

contains

  ! ---------------------------------------------------------
  subroutine states_elec_dim_copy(dout, din)
    type(states_elec_dim_t), intent(inout) :: dout
    type(states_elec_dim_t), intent(in)    :: din

    PUSH_SUB(states_elec_dim_copy)

    call states_elec_dim_end(dout)

    dout%dim            = din%dim
    dout%ispin          = din%ispin
    dout%nspin          = din%nspin
    dout%spin_channels  = din%spin_channels


    call distributed_copy(din%kpt, dout%kpt)

    POP_SUB(states_elec_dim_copy)
  end subroutine states_elec_dim_copy


  ! ---------------------------------------------------------
  subroutine states_elec_dim_end(dim)
    type(states_elec_dim_t), intent(inout) :: dim

    PUSH_SUB(states_elec_dim_end)

    call distributed_end(dim%kpt)


    POP_SUB(states_elec_dim_end)
  end subroutine states_elec_dim_end


  ! ---------------------------------------------------------
  !> Returns true if k-point ik denotes spin-up, in spin-polarized case.
  !
  logical pure function is_spin_up(ik)
    integer, intent(in) :: ik

    is_spin_up = odd(ik)

  end function is_spin_up


  ! ---------------------------------------------------------
  !> Returns true if k-point ik denotes spin-down, in spin-polarized case.
  !
  logical pure function is_spin_down(ik)
    integer, intent(in) :: ik

    is_spin_down = even(ik)

  end function is_spin_down

  ! ---------------------------------------------------------
  !> extract the spin index from the combined spin and k index
  !
  integer pure function states_elec_dim_get_spin_index(this, iq) result(index)
    class(states_elec_dim_t), intent(in) :: this
    integer,                  intent(in) :: iq

    if (this%ispin == SPIN_POLARIZED) then
      index = 1 + mod(iq - 1, 2)
    else
      index = 1
    end if

  end function states_elec_dim_get_spin_index


  ! ---------------------------------------------------------
  !> extract the k-point index from the combined spin and k index
  !
  integer pure function states_elec_dim_get_kpoint_index(this, iq) result(index)
    class(states_elec_dim_t), intent(in) :: this
    integer,                  intent(in) :: iq

    if (this%ispin == SPIN_POLARIZED) then
      index = 1 + (iq - 1)/2
    else
      index = iq
    end if

  end function states_elec_dim_get_kpoint_index




end module states_elec_dim_oct_m


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
