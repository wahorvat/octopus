set term png size 600,600

set output 'tutorial_04.1-drude-02.png'
set xlabel 'time step'
set ylabel 'J_z'
set size square
plot 'NP/td.general/current_at_points' u 1:5 w l title "before", 'NP/td.general/current_at_points' u 1:8 w l title "middle", 'NP/td.general/current_at_points' u 1:11 w l title "after"

set output 'tutorial_04.1-drude-03.png'
set xlabel 'time step'
set ylabel 'E_z'
set size square
plot 'Maxwell/td.general/total_e_field_z' u 1:3 w l title "before", 'Maxwell/td.general/total_e_field_z' u 1:4 w l title "middle", 'Maxwell/td.general/total_e_field_z' u 1:5 w l title "after"

