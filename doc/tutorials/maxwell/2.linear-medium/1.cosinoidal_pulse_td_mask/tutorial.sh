#!/usr/bin/env bash

# these variables need to be defined:
# HELPER_DIR=~/HUGO/octopus-documentation/scripts/
# OCTOPUS_TOP=~/Octopus/octopus/

rm *.log *.txt


octopus > log
gnuplot plot1.gnu

cp inp *.txt *.gnu tutorial.sh lens.off lens.scad $OCTOPUS_TOP/doc/tutorials/maxwell/2.linear-medium/1.cosinoidal_pulse_td_mask/

cp *.png $OCTOPUS_TOP/doc/html/images/Maxwell/
