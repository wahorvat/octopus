#!/bin/bash
module load cray-fftw gsl cray-netcdf-hdf5parallel parpack

./configure --prefix=`pwd` --with-libxc-prefix=/usr/common/usg/libxc/3.0.0/intel/ivybridge --enable-mpi \
CC=cc CXX=CC FC=ftn FCFLAGS="-fast -no-ipo" CFLAGS="-fast -no-ipo" \
FCCPP="/lib/cpp -ffreestanding" --with-fftw-prefix=$FFTW_DIR/..
--with-arpack="$ARPACK" --with-parpack=no \
--with-berkeleygw-prefix=/usr/common/software/berkeleygw/2.0
make -j 6 install
