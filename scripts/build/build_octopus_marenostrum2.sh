#!/bin/bash

export LD_LIBRARY_PATH=/usr/lib
export CC=mpicc
export FC=mpif90

export CFLAGS="-q64 -O3 -qtune=ppc970 -qarch=ppc970 -qcache=auto -qnostrict -qignerrno -qinlglue"
export FCFLAGS=$CFLAGS" -qessl -qxlf90=autodealloc"

export LIBS_BLAS="-L/usr/lib64 -lessl"

./configure \
--with-lapack=/gpfs/apps/LAPACK/lib64/liblapack.a \
--disable-gsltest \
--disable-gdlib \
--with-gsl-prefix=$outdirGSL \
--with-fft-lib=$outdirFFT/lib/libfftw3.a \
--prefix=$outdir --enable-mpi --disable-f90-forall
