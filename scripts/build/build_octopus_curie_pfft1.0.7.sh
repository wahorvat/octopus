 #!/bin/bash

module load c/intel
module load fortran/intel
module load bullxmpi
module load mkl
module load gsl/1.14

WPREFIX=$WORKDIR/poisson/software
WLPREFIX=$WORKDIR/poisson/local
export ISF_HOME="$WLPREFIX/libisf"
export FFTW_HOME="$WLPREFIX/fftw-3.3.3"
export PFFT_HOME="$WLPREFIX/pfft-1.0.7-alpha"

export CC=mpicc
export CFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$ISF_HOME/include"
export FC=mpif90
export FCFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$FFTW_HOME/include -I$ISF_HOME/include"

export LIBS_BLAS="${MKL_LIBS}"
export LIBS_FFT="-Wl,--start-group -L$PFFT_HOME/lib -L$FFTW_HOME/lib -lpfft -lfftw3 -lfftw3_mpi -Wl,--end-group"

export LDFLAGS=" -L/usr/lib64 -L$ISF_HOME/lib -lPSolver-1 -lrt"
export LD_LIBRARY_PATH=$ISF_HOME/lib:$LD_LIBRARY_PATH

../configure --prefix=$WPREFIX/octopus/superciliosus_pfft \
--disable-openmp --with-libxc-prefix=$WPREFIX/libxc/11066 \
--disable-gdlib --with-gsl-prefix=/usr/local/gsl-1.14 \
--enable-mpi --enable-newuoa \
--with-pfft-prefix=$PFFT_HOME
