# !1888 TODO: To be removed and switched to submodules

add_library(bpdn STATIC
		bpdn/expmm.c
		bpdn/heap.c
		bpdn/oneProjectorCore.c
		bpdn/spgl1_projector.c
		bpdn/bpdn.f90)
target_include_directories(bpdn PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(bpdn INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})
add_library(dftd3 STATIC
		dftd3/api.f90
		dftd3/common.f90
		dftd3/core.f90
		dftd3/pars.f90
		dftd3/sizes.f90)
target_include_directories(dftd3 PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(dftd3 INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/dftd3/pars.dat DESTINATION ${PROJECT_BINARY_DIR}/share/dftd3/
		REGEX "(CMakeLists.txt|\\.am$)" EXCLUDE)
add_library(qshep STATIC
		qshep/qshep2d.f90
		qshep/qshep3d.f90)
target_include_directories(qshep PRIVATE
		${CMAKE_CURRENT_BINARY_DIR}/../src/include)
target_include_directories(qshep INTERFACE
		${CMAKE_CURRENT_BINARY_DIR})

add_library(rapidxml INTERFACE)
target_include_directories(rapidxml INTERFACE rapidxml)